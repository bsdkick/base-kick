#!/bin/sh

freebsd_update()
{
	info "freebsd-update fetch"
	freebsd-update fetch | grep NOTHING
	info "freebsd-update install"
	freebsd-update install --not-running-from-cron | grep NOTHING
}
