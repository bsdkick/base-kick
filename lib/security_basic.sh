#!/bin/sh

security_basic()
{
	info "Clearing /etc/motd"
	rm -f /etc/motd
	touch /etc/motd

	info "Adding do not update motd to /etc/rc.conf"
	echo 'update_motd="NO"' >> /etc/rc.conf
	
	
#	http://bsdadventures.com/harden-freebsd/
	info "Disallow root console login in /etc/ttys"
	sed -i -- 's/ secure/ insecure/g' /etc/ttys

	info "Stealth network with blackholes added /etc/sysctl.conf"
	echo 'net.inet.tcp.blackhole=2' >> /etc/sysctl.conf
	echo 'net.inet.udp.blackhole=1' >> /etc/sysctl.conf



}
