#!/bin/sh

readonly LOG_FILE="base-kick.log"
info()    { 
	if [ ! "$1" = '' ]; then
		echo "$(date -u) [INFO]    $*" | tee -a "$LOG_FILE" >&2 ;
	fi
	}
warning() { echo "[WARNING] $*" | tee -a "$LOG_FILE" >&2 ; }
error()   { echo "[ERROR]   $*" | tee -a "$LOG_FILE" >&2 ; }
fatal()   { echo "[FATAL]   $*" | tee -a "$LOG_FILE" >&2 ; exit 1 ; }

