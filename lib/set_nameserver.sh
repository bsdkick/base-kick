#!/bin/sh

set_nameservers()
{
	# Nameserver from conf/settings.sh; : Default fallback if not given
	  
	_NAMESERVER1="$NAMESERVER1"; 	: "${_NAMESERVER1:=8.8.8.8}"
	_NAMESERVER2="$NAMESERVER2";	: "${_NAMESERVER2:=8.8.4.4}"
	
	info "Setting nameservers to: $_NAMESERVER1, $_NAMESERVER2"
	
	echo "nameserver $_NAMESERVER1" > /etc/resolv.conf
	echo "nameserver $_NAMESERVER2" >> /etc/resolv.conf
}