#!/bin/sh

init_pkg() {
	info "Installing pkg tool"
	env ASSUME_ALWAYS_YES=YES pkg install -y pkg 2>&1 | grep NOTHING
}

install_pkg()
{
	info "pkg install $1"
	env ASSUME_ALWAYS_YES=YES pkg install -y "$1" 2>&1 | grep NOTHING
}

install_pkgs()
{
	# install all pkgs in group
	info "Loading pkg_group/$1"
	pkgs=$(cat "pkg_group/$1")
	for pkg in $pkgs; do
		install_pkg "$pkg"
	done
	
	# post installation
	if [ -e "pkg_group/$1.post.sh" ]; then
		info "Running post pkg_group/$1 install operations"
		"pkg_group/$1.post.sh"
	fi
}