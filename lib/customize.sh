#!/bin/sh

customize() {
	info "Customize stuff"
	chmod -x /usr/bin/fortune
	
	echo 'accf_http_load="YES"' >> /boot/loader.conf
	kldload accf_http
	
	echo 'accf_data_load="YES"' >> /boot/loader.conf
	kldload accf_data
	
	cat <<____EndOfLoginConf > /root/.login_conf
me:\
	:charset=UTF-8:\
	:lang=en_US.UTF-8:	
____EndOfLoginConf

	fetch https://www.gujord.com/rc/_vimrc
	mv _vimrc /root/.vimrc
}