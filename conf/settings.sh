#!/bin/sh


# Customize jails in iocage.post.sh

# settings

export ADMIN_EMAIL=admin@example.local

# Network
export JAIL_IPNET="10.0.3"

# DNS
export NAMESERVER1=8.8.8.8
export NAMESERVER2=8.8.4.4

# jail old web application firewall
export CREATE_JAIL_WAF_OLD="NO"
export WAF_OLD_NAME="waf"
export WAF_OLD_HOSTNAME="waf.local"
export WAF_OLD_IP="$JAIL_IPNET.10/32"

# jail debian jessie stable
export CREATE_JAIL_LINUX_OLD="NO"
export LINUX_OLD_NAME="linuxold"
export LINUX_OLD_HOSTNAME="linuxold.local"
export LINUX_OLD_IP="$JAIL_IPNET.20/32"

# jail FreeBSD MySQL 55
export CREATE_MYSQL55="NO"
export MYSQL55_NAME="mysql55"
export MYSQL55_HOSTNAME="mysql55.local"
export MYSQL55_IP="$JAIL_IPNET.30/32"

