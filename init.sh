#!/bin/sh

# Work from own directory
cd "$(dirname "$0")" || exit

# Clean up when done or failed
. lib/cleanup.sh; trap cleanup EXIT

# Custom defaults settings
. conf/settings.sh

# Functions
. lib/log.sh; info "Starting base-kick install"
. lib/set_nameserver.sh
. lib/freebsd_update.sh
. lib/install_pkg.sh
. lib/security_basic.sh
. lib/customize.sh

# ----------------------------------------------------------------------------

# Networking
	set_nameservers

# OS Preps
	freebsd_update
	init_pkg

# Customize stuff
	customize

# Install stuff
	install_pkgs base

	# TESTING iocage
	install_pkgs iocage

# Configure stuff			
	security_basic
	
	# Last step : Security - file integrety snapshot
	install_pkgs security

exit