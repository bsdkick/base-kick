#!/bin/sh

#
# DRAFT
#




zfs create -o mountpoint=/usr/jails zroot/usr/jails
zfs create zroot/usr/jails/deb-master
mkdir /usr/jails/etc
kldload fdescfs linprocfs linsysfs tmpfs linux linux64

cat <<EndOfBootLoaderConf >> /boot/loader.conf
fdescfs_load="YES"
linprocfs_load="YES"
linsysfs_load="YES"
tmpfs_load="YES"
EndOfBootLoaderConf

echo 'jail_enable="YES"' >> /etc/rc.conf

cat <<EndOfJailMounts >> /usr/jails/etc/fstab.deb-master
linsys   /usr/jails/deb-master/sys         linsysfs  rw          0 0
linproc  /usr/jails/deb-master/proc        linprocfs rw          0 0
EndOfJailMounts
#tmpfs    /usr/jails/deb-master/lib/init/rw tmpfs     rw,mode=777 0 0

cat <<EndOfJailConf >> /etc/jail.conf
deb-master {
  path = /usr/jails/deb-master;
  allow.mount;
  mount.devfs;
  host.hostname = deb-master;
  mount.fstab="/usr/jails/etc/fstab.deb-master";
  ip4.addr = 127.0.0.10;
  interface = lo0;
  exec.start = "/etc/init.d/rc 3";
  exec.stop = "/etc/init.d/rc 0";
}
EndOfJailConf

#debootstrap --foreign --arch=i386 jessie /usr/jails/deb-master ftp://ftp.uninett.no/pub/linux/debian/
debootstrap --foreign --arch=amd64 jessie /usr/jails/deb-master ftp://ftp.uninett.no/pub/linux/debian/


mount -t linprocfs none /usr/jails/deb-master/proc
mount -t devfs none /usr/jails/deb-master/dev
mount -t linsysfs none /usr/jails/deb-master/sys
# mount -t tmpfs none /usr/jails/deb-master/lib/init/rw


rm -f /usr/jails/deb-master/var/cache/apt/archives/sysvinit-utils_2.88dsf-59_amd64.deb
rm -f /usr/jails/deb-master/var/cache/apt/archives/rsyslog_8.4.2-1+deb8u2_amd64.deb













exit











