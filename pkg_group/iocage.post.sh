#!/bin/sh

. lib/log.sh

# IOCAGE from source (broken 20171017)
#cd /root/ || exit
#git clone --recursive https://github.com/iocage/iocage
#cd iocage || exit
#make
#make install


##############################################################################
# IOCAGE
#
cat <<____EndOfSysctl >> /etc/sysctl.conf
security.jail.mount_zfs_allowed=0
security.jail.mount_tmpfs_allowed=1
security.jail.mount_linsysfs_allowed=1
security.jail.mount_linprocfs_allowed=1
security.jail.mount_procfs_allowed=1
security.jail.mount_nullfs_allowed=1
security.jail.mount_fdescfs_allowed=1
security.jail.mount_devfs_allowed=1
security.jail.mount_allowed=0
security.jail.chflags_allowed=1
security.jail.allow_raw_sockets=1
security.jail.sysvipc_allowed=1
security.jail.set_hostname_allowed=1
____EndOfSysctl

info "Setting sysctl values for jails"
sysctl security.jail.mount_tmpfs_allowed=1 2>&1 | grep NOTHING
sysctl security.jail.mount_linsysfs_allowed=1 2>&1 | grep NOTHING
sysctl security.jail.mount_linprocfs_allowed=1 2>&1 | grep NOTHING
sysctl security.jail.mount_procfs_allowed=1 2>&1 | grep NOTHING
sysctl security.jail.mount_nullfs_allowed=1 2>&1 | grep NOTHING
sysctl security.jail.mount_fdescfs_allowed=1 2>&1 | grep NOTHING
sysctl security.jail.mount_devfs_allowed=1 2>&1 | grep NOTHING
sysctl security.jail.chflags_allowed=1 2>&1 | grep NOTHING
sysctl security.jail.allow_raw_sockets=1 2>&1 | grep NOTHING
sysctl security.jail.sysvipc_allowed=1 2>&1 | grep NOTHING
sysctl security.jail.set_hostname_allowed=1 2>&1 | grep NOTHING

info "Setting iocage to start on boot"
sysrc iocage_enable=YES 2>&1 | grep NOTHING
kldload linux64 tmpfs linprocfs linsysfs
echo 'linux64_load="YES"' >> /boot/loader.conf
echo 'tmpfs_load="YES"' >> /boot/loader.conf 
echo 'linprocfs_load="YES"' >> /boot/loader.conf 
echo 'linsysfs_load="YES"' >> /boot/loader.conf
mount -t fdescfs fdesc /dev/fd
echo 'fdesc /dev/fd  fdescfs  rw  0  0' >> /etc/fstab

# iocage bug fix
	info "iocage: Patching myself ;-)"
	fetch https://raw.githubusercontent.com/igalic/iocage/d4f515ec711214ef6df69abc08607dfa7bc69964/iocage/lib/ioc_fetch.py 2>&1 | grep NOTHING
	cat ioc_fetch.py > /usr/local/lib/python3.6/site-packages/iocage/lib/ioc_fetch.py
# /iocage bug fix

# zsh completion
fetch https://raw.githubusercontent.com/iocage/iocage/master/zsh-completion/_iocage
mv _iocage /usr/local/share/zsh/site-functions


IFACE=$(ifconfig | grep 'flags=' | grep -v LOOPBACK | awk '{ print $1 }' | sed 's/://g')

##############################################################################
# IOCAGE : wafold : FreeBSD Web Application Firewall
#
if [ "$CREATE_JAIL_WAF_OLD" = "YES" ]; then
	RELEASE=$(uname -r)
	info "iocage: creating FreeBSD $RELEASE jail: $WAF_OLD_NAME - $WAF_OLD_IP"
	iocage create -r "$RELEASE" --name "$WAF_OLD_NAME" boot=on 2>&1 | grep NOTHING
	iocage set "ip4_addr=$IFACE|$WAF_OLD_IP" "$WAF_OLD_NAME" 2>&1 | grep NOTHING
	info "iocage: Installing packages in $WAF_OLD_NAME"	
	iocage chroot "$WAF_OLD_NAME" env ASSUME_ALWAYS_YES=YES pkg install -y zsh curl git vim-lite 2>&1 | grep NOTHING
	iocage chroot "$WAF_OLD_NAME" 'curl -fsSL https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh | /bin/sh' 2>&1 | grep NOTHING
	iocage exec "$WAF_OLD_NAME" 'echo "export ZSH=/root/.oh-my-zsh" > /root/.zshrc' 2>&1 | grep NOTHING
	iocage exec "$WAF_OLD_NAME" "echo 'ZSH_THEME=\"steeef\"' >> /root/.zshrc" 2>&1 | grep NOTHING
	iocage exec "$WAF_OLD_NAME" "echo 'plugins=(git)' >> /root/.zshrc" 2>&1 | grep NOTHING
	iocage exec "$WAF_OLD_NAME" "echo 'source \$ZSH/oh-my-zsh.sh' >> /root/.zshrc" 2>&1 | grep NOTHING
	iocage exec "$WAF_OLD_NAME" "echo 'export LANG=en_US.UTF-8' >> /root/.zshrc"
	iocage exec "$WAF_OLD_NAME" "sed -i -- 's/root:\/bin\/csh/root:\/usr\/local\/bin\/zsh/g' /etc/passwd" 2>&1 | grep NOTHING
	iocage exec "$WAF_OLD_NAME" "sed -i -- 's/root:\/bin\/csh/root:\/usr\/local\/bin\/zsh/g' /etc/master.passwd" 2>&1 | grep NOTHING
	iocage exec "$WAF_OLD_NAME" "pwd_mkdb -p /etc/master.passwd" 2>&1 | grep NOTHING
	iocage exec "$WAF_OLD_NAME" /etc/periodic/weekly/310.locate 2>&1 | grep NOTHING
	
	iocage set host_hostname="$WAF_OLD_HOSTNAME" "$WAF_OLD_NAME"
	echo "$WAF_OLD_HOSTNAME" > "/iocage/jails/$WAF_OLD_NAME/root/etc/hostname"
	
#	iocage exec wafold sysrc sshd_enable="YES"
#	iocage exec wafold service sshd start
	info "iocage: $WAF_OLD_NAME is ready."
fi

##############################################################################
# IOCAGE : mysql55 : FreeBSD Web Application Firewall
#
if [ "$CREATE_JAIL_MYSQL55" = "YES" ]; then
	info "iocage: creating FreeBSD $RELEASE jail: $MYSQL55_NAME - $MYSQL55_IP"
	iocage create -r "$RELEASE" --name "$MYSQL55_NAME" boot=on 2>&1 | grep NOTHING
	iocage set "ip4_addr=$IFACE|$MYSQL55_IP" "$MYSQL55_NAME" 2>&1 | grep NOTHING
	info "iocage: Installing packages in $MYSQL55_NAME"
	iocage chroot "$MYSQL55_NAME" env ASSUME_ALWAYS_YES=YES pkg install -y zsh curl git vim-lite 2>&1 | grep NOTHING
	iocage chroot "$MYSQL55_NAME" env ASSUME_ALWAYS_YES=YES pkg install -y mysql55-server 2>&1 | grep NOTHING
	iocage chroot "$MYSQL55_NAME" sysrc mysql_enable=YES 2>&1 | grep NOTHING
	iocage chroot "$MYSQL55_NAME" 'curl -fsSL https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh | /bin/sh' 2>&1 | grep NOTHING
	iocage exec "$MYSQL55_NAME" 'echo "export ZSH=/root/.oh-my-zsh" > /root/.zshrc' 2>&1 | grep NOTHING
	iocage exec "$MYSQL55_NAME" "echo 'ZSH_THEME=\"steeef\"' >> /root/.zshrc" 2>&1 | grep NOTHING
	iocage exec "$MYSQL55_NAME" "echo 'plugins=(git)' >> /root/.zshrc" 2>&1 | grep NOTHING
	iocage exec "$MYSQL55_NAME" "echo 'export LANG=en_US.UTF-8' >> /root/.zshrc"
	iocage exec "$MYSQL55_NAME" "echo 'source \$ZSH/oh-my-zsh.sh' >> /root/.zshrc" 2>&1 | grep NOTHING
	iocage exec "$MYSQL55_NAME" "sed -i -- 's/root:\/bin\/csh/root:\/usr\/local\/bin\/zsh/g' /etc/passwd" 2>&1 | grep NOTHING
	iocage exec "$MYSQL55_NAME" "sed -i -- 's/root:\/bin\/csh/root:\/usr\/local\/bin\/zsh/g' /etc/master.passwd" 2>&1 | grep NOTHING
	iocage exec "$MYSQL55_NAME" "pwd_mkdb -p /etc/master.passwd" 2>&1 | grep NOTHING
	iocage exec "$MYSQL55_NAME" /etc/periodic/weekly/310.locate 2>&1 | grep NOTHING
	iocage set host_hostname="$MYSQL55_HOSTNAME" "$MYSQL55_NAME"
	echo "$MYSQL55_HOSTNAME" > "/iocage/jails/$MYSQL55_NAME/root/etc/hostname"
	info "iocage: $MYSQL55_NAME is ready."
fi

##############################################################################
# IOCAGE : app : FreeBSD
#
if [ "$CREATE_JAIL_APP" = "YES" ]; then
	info "iocage: creating FreeBSD $RELEASE jail: $APP_NAME - $APP_IP" 
	iocage create -r "11.1-RELEASE" --name "app" boot=on 2>&1 | grep NOTHING
	iocage set "ip4_addr=$IFACE|$APP_IP" "$APP_NAME" 2>&1 | grep NOTHING
	info "iocage: Installing packages in $APP_NAME"
	iocage chroot "$APP_NAME" env ASSUME_ALWAYS_YES=YES pkg install -y zsh curl git vim-lite 2>&1 | grep NOTHING
	iocage chroot "$APP_NAME" env ASSUME_ALWAYS_YES=YES pkg install -y mysql55-server 2>&1 | grep NOTHING
	iocage chroot "$APP_NAME" sysrc mysql_enable=YES 2>&1 | grep NOTHING
	iocage chroot "$APP_NAME" 'curl -fsSL https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh | /bin/sh' 2>&1 | grep NOTHING
	iocage exec "$APP_NAME" 'echo "export ZSH=/root/.oh-my-zsh" > /root/.zshrc' 2>&1 | grep NOTHING
	iocage exec "$APP_NAME" "echo 'ZSH_THEME=\"steeef\"' >> /root/.zshrc" 2>&1 | grep NOTHING
	iocage exec "$APP_NAME" "echo 'plugins=(git)' >> /root/.zshrc" 2>&1 | grep NOTHING
	iocage exec "$APP_NAME" "echo 'export LANG=en_US.UTF-8' >> /root/.zshrc"
	iocage exec "$APP_NAME" "echo 'source \$ZSH/oh-my-zsh.sh' >> /root/.zshrc" 2>&1 | grep NOTHING
	iocage exec "$APP_NAME" "sed -i -- 's/root:\/bin\/csh/root:\/usr\/local\/bin\/zsh/g' /etc/passwd" 2>&1 | grep NOTHING
	iocage exec "$APP_NAME" "sed -i -- 's/root:\/bin\/csh/root:\/usr\/local\/bin\/zsh/g' /etc/master.passwd" 2>&1 | grep NOTHING
	iocage exec "$APP_NAME" "pwd_mkdb -p /etc/master.passwd" 2>&1 | grep NOTHING
	iocage exec "$APP_NAME" /etc/periodic/weekly/310.locate 2>&1 | grep NOTHING
	info "iocage: $APP_NAME is ready."
fi



##############################################################################
# IOCAGE : linuxold : Debian jessie
#
if [ "$CREATE_JAIL_LINUX_OLD" = "YES" ]; then
	info "iocage: creating Debian Jessie jail: $LINUX_OLD_NAME - $LINUX_OLD_IP"
	iocage create -e -n "$LINUX_OLD_NAME" boot=on exec_start="/etc/init.d/rc 3" exec_stop="/etc/init.d/rc 0"
	debootstrap --foreign --arch=amd64 jessie "/iocage/jails/$LINUX_OLD_NAME/root" http://deb.debian.org/debian/
	cat <<____EndOfSourcesList > "/iocage/jails/$LINUX_OLD_NAME/root/etc/apt/sources.list"
	deb http://ftp.no.debian.org/debian jessie main contrib non-free
	deb http://ftp.no.debian.org/debian jessie-updates main contrib non-free
	deb http://security.debian.org/debian-security jessie/updates main contrib non-free
____EndOfSourcesList
	iocage get mountpoint "$LINUX_OLD_NAME"
	cat <<____EndOfFStab > "/iocage/jails/$LINUX_OLD_NAME/fstab"
	linsys   /iocage/jails/$LINUX_OLD_NAME/root/sys         linsysfs  rw          0 0
	linproc  /iocage/jails/$LINUX_OLD_NAME/root/proc        linprocfs rw          0 0
____EndOfFStab
	rm -f "/iocage/jails/$LINUX_OLD_NAME/root/var/cache/apt/archives/rsyslog_8.4.2-1+deb8u2_amd64.deb"
	IFACE=$(ifconfig | grep 'flags=' | grep -v LOOPBACK | awk '{ print $1 }' | sed 's/://g')
	iocage set "ip4_addr=$IFACE|$LINUX_OLD_IP" "$LINUX_OLD_NAME"
	iocage exec "$LINUX_OLD_NAME" 'env DEBIAN_FRONTEND=noninteractive dpkg --force-depends -Eia /var/cache/apt/archives/*.deb'
	iocage chroot "$LINUX_OLD_NAME" apt-get update
	iocage chroot "$LINUX_OLD_NAME" apt-get dist-upgrade -y
	iocage chroot "$LINUX_OLD_NAME" apt-get install -y zsh curl git
	iocage set host_hostname="$LINUX_OLD_HOSTNAME" "$LINUX_OLD_NAME"
	echo "$LINUX_OLD_HOSTNAME" > "/iocage/jails/$LINUX_OLD_NAME/root/etc/hostname"
	
	# Dev server:
	iocage chroot "$LINUX_OLD_NAME" apt-get install -y gcc libxml2-dev libssl-dev libcurl4-openssl-dev libpcre3-dev libbz2-dev libjpeg-dev libpng12-dev libfreetype6-dev libxpm-dev libgd2-xpm-dev libmcrypt-dev libmhash-dev libmysqlclient-dev libpspell-dev libxslt1-dev libltdl-dev build-essential libaprutil1-dev mysql-client-5.5
	iocage exec "$LINUX_OLD_NAME" 'ln -s /usr/include/freetype2 /usr/include/freetype2/freetype'
	# /Dev server
	
	iocage chroot "$LINUX_OLD_NAME" 'curl -fsSL https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh | /bin/sh'
	iocage exec "$LINUX_OLD_NAME" 'echo "export ZSH=/root/.oh-my-zsh" > /root/.zshrc'
	iocage exec "$LINUX_OLD_NAME" "echo 'ZSH_THEME=\"steeef\"' >> /root/.zshrc"
	iocage exec "$LINUX_OLD_NAME" "echo 'plugins=(git)' >> /root/.zshrc"
	iocage exec "$LINUX_OLD_NAME" "echo 'source \$ZSH/oh-my-zsh.sh' >> /root/.zshrc"
	iocage exec "$LINUX_OLD_NAME" "echo 'export LANG=en_US.UTF-8' >> /root/.zshrc"
	iocage exec "$LINUX_OLD_NAME" "sed -i -- 's/root:\/bin\/bash/root:\/bin\/zsh/g' /etc/passwd"
	iocage exec "$LINUX_OLD_NAME" "sed -i -- 's/root:\/bin\/bash/root:\/bin\/zsh/g' /etc/passwd-"
	iocage exec "$LINUX_OLD_NAME" 'ln -s /bin/zsh /usr/local/bin/zsh'
	iocage exec "$LINUX_OLD_NAME" 'ln -s /usr/bin/vim.tiny /usr/bin/vim'
	
	# Apache server
	iocage chroot "$LINUX_OLD_NAME" apt-get install -y lynx
	iocage chroot "$LINUX_OLD_NAME" wget http://apache.uib.no//httpd/httpd-2.2.34.tar.gz --output-document /root/httpd-2.2.34.tar.gz
	iocage chroot "$LINUX_OLD_NAME" tar xvf /root/httpd-2.2.34.tar.gz -C /root/
	iocage chroot "$LINUX_OLD_NAME" 'cd /root/httpd-2.2.34/ && ./configure --enable-layout=Debian --enable-so --with-program-name=apache2  --with-ldap=yes --with-ldap-include=/usr/include --with-ldap-lib=/usr/lib --with-suexec-caller=www-data --with-suexec-bin=/usr/lib/apache2/suexec --with-suexec-docroot=/var/www --with-suexec-userdir=public_html --with-suexec-logfile=/var/log/apache2/suexec.log --with-suexec-uidmin=100 --enable-suexec=shared --enable-log-config=static --enable-logio=static --with-apr=/usr/bin/apr-1-config --with-apr-util=/usr/bin/apu-1-config --with-pcre=yes --enable-pie --enable-authn-alias=shared --enable-authnz-ldap=shared --enable-disk-cache=shared --enable-cache=shared --enable-mem-cache=shared --enable-file-cache=shared --enable-cern-meta=shared --enable-dumpio=shared --enable-ext-filter=shared --enable-charset-lite=shared --enable-cgi=shared --enable-dav-lock=shared --enable-log-forensic=shared --enable-ldap=shared --enable-proxy=shared --enable-proxy-connect=shared --enable-proxy-ftp=shared --enable-proxy-http=shared --enable-proxy-ajp=shared --enable-proxy-scgi=shared --enable-proxy-balancer=shared --enable-ssl=shared --enable-authn-dbm=shared --enable-authn-anon=shared --enable-authn-dbd=shared --enable-authn-file=shared --enable-authn-default=shared --enable-authz-host=shared --enable-authz-groupfile=shared --enable-authz-user=shared --enable-authz-dbm=shared --enable-authz-owner=shared --enable-authnz-ldap=shared --enable-authz-default=shared --enable-auth-basic=shared --enable-auth-digest=shared --enable-dbd=shared --enable-deflate=shared --enable-include=shared --enable-filter=shared --enable-env=shared --enable-mime-magic=shared --enable-expires=shared --enable-headers=shared --enable-ident=shared --enable-usertrack=shared --enable-unique-id=shared --enable-setenvif=shared --enable-version=shared --enable-status=shared --enable-autoindex=shared --enable-asis=shared --enable-info=shared --enable-cgid=shared --enable-dav=shared --enable-dav-fs=shared --enable-vhost-alias=shared --enable-negotiation=shared --enable-dir=shared --enable-imagemap=shared --enable-actions=shared --enable-speling=shared --enable-userdir=shared --enable-alias=shared --enable-rewrite=shared --enable-mime=shared --enable-substitute=shared  --enable-reqtimeout=shared'
	iocage chroot "$LINUX_OLD_NAME" 'cd /root/httpd-2.2.34/ && make -j "$(nproc)"'
	iocage chroot "$LINUX_OLD_NAME" 'cd /root/httpd-2.2.34/ && make install'
	iocage exec "$LINUX_OLD_NAME" sed -i -- 's/ServerRoot ""/ServerRoot "\/"/g' /etc/apache2/apache2.conf
	iocage exec "$LINUX_OLD_NAME" "echo 'Include /etc/apache2/httpd.conf' >> /etc/apache2/apache2.conf"
	iocage exec "$LINUX_OLD_NAME" "echo \"ServerName $LINUX_OLD_NAME\" >> /etc/apache2/httpd.conf"
	iocage exec "$LINUX_OLD_NAME" mkdir /var/www
	mkdir /etc/apache2/sites-available
	mkdir /etc/apache2/sites-enabled
	
	# /Apache server
	
	# Postfix Mail server
	iocage chroot "$LINUX_OLD_NAME" env DEBIAN_FRONTEND=noninteractive apt-get install -y postfix
	# /Postfix Mail server
fi

