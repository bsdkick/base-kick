#!/bin/sh

. lib/log.sh

# 1. edit /usr/local/etc/aide.conf
cd /var/db/aide || exit
info "Creating file integrity snapshot"
aide --init 2>&1 | grep NOTHING
mv databases/aide.db.new databases/aide.db 2>&1 | info
